## Alexandria Books

Alexandria Books is an eBook aspires to be a server and swiss army knife for
all your eBook needs. It is built on a plugin system that allows any component
to be swapped out for another component. There are many plugins already
available that provide a web server, eBook transcoding (automatic conversion),
and source plugins for adding new eBooks to your collection!

The goal of Alexandria Books is to allow you to read and enjoy your eBooks and
comics on the devices you already have with the apps you already use.

Use the links above to [get started](page/running/), or see the code hosted on GitLab: https://gitlab.com/alexandriabooks/
