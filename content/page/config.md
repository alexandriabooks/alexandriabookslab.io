---
title: Configuration
subtitle: How to customize Alexandria Books for your needs
comments: false
---

Configuration can be done through an `alexandria.yml` file and/or
through environment variables. Environment variables will override
entries in the config file.

### Configuration File

The `alexandria.yml` file should be in the working directory, or
specified via the `ALEXANDRIABOOKSCONFIG` environment variable.

All entries in the config file are normalized into their environment
variable equivalent.

    cache:
        plugin: alexandriabooks_local.cache.FileBasedCacheService
    #local:
    #    directory: /mnt/eBooks/

The above file would create a single configuration entry:
   `CACHE_PLUGIN`
with the value
   `alexandriabooks_local.cache.FileBasedCacheService`.

The `local/directory` configuration is commented with a `#` so it is ignored.

### Environment Variable Configuration

Configuration may also be specified with environment variables. All
configuration via the environment is prefixed with `ALEXANDRIABOOKS_`, which
is stripped off during processing.

The above configuration could also be specified in `bash` via:

    export ALEXANDRIABOOKS_CACHE_PLUGIN="alexandriabooks_local.cache.FileBasedCacheService"

### Profiles

Profiles are separate configuration that may be activated and layered
upon the base configuration detailed above.

They may be activated via the REST api or LoFi interface by specifing
the query parameter `profile={profile name}`. The `profile name` may
only contain the latin letters a-z and numbers 0-9, and must no more
than 16 characters in length.

Profiles are typically loaded from YAML files, where the file is named
`alexandria_profile.{profile name}.yml` (lowercase profile name) and
follow the same format as the YAML configuration file above. Although
they may also be loaded from the environment, similar to the environment
config details above, but with the prefix:
`ALEXANDRIABOOKSPROFILE_{PROFILE NAME}_`. Note that the profile name is
uppercased for the environment.

Profiles may be activated in the `lofi` interface by adding a
`?profile={PROFILE NAME}` to the URL.

### Auto-activation of Profiles

Profiles may be also auto-activated in web/rest calls based on a regex of the
calling application's User Agent string. See configuration example below:

    web:
        profile_activation:
            agent_string:
                kobo: '.*Kobo Touch.*'

This activates the profile named `kobo` when the User Agent string contains
`Kobo Touch`.

### Configuration Values

**Note:** Configuration here will be shown in dot notation.
Translation into YAML requires each dot separated value be a level.
If specifying configuration through environment variables, the name
should be uppercased, prefixed with `ALEXANDRIABOOKS_` and the dots (`.`)
replaced with underscores (`_`).

* **metadata.plugin** - Chooses a cache plugin. Defaults to:
`alexandriabooks_core.services.simple.InMemoryBookMetadataService`.
* **metadata.as_indexer** - If the metadata storage is directly searchable, and another indexer has already been registered, still allow the metadata store to be searched.
* **cache.plugin** - Chooses a cache plugin. Defaults to:
`alexandriabooks_core.services.simple.NoOpCache`, may also be `alexandriabooks_local.cache.FileBasedCacheService` which uses a local `cache` directory in the working directory to store the persistent cache.
* **cache.directory** - Changes the cache directory. Defaults to `cache` in the working directory.
* **local.directory** - Sets a local directory of eBooks to serve.
* **web.hifi.path** - Enable the hifi interface, and sets the path to load it from.
* **web.lofi.page_size** - Number of results per page.
* **web.lofi.supported_types** - Comma separated list of supported file types.
* **web.rest.image.max_w** - Maximum width of image to return from server (includes cover images and book transcodes).
* **web.rest.image.max_h** - Maximum height of image to return from server (includes cover images and book transcodes).
* **web.rest.image.thumb_w** - Maximum width of image to return from server for thumbnails.
* **web.rest.image.thumb_h** - Maximum height of image to return from server for thumbnails.
* **web.rest.image.type** - Type of image to return from the server (includes cover images and book transcodes).
* **web.rest.image.greyscale** - True to convert the image to greyscale (includes cover images and book transcodes).
* **sharing.port** - Enable server sharing hosting on the specified port.
* **sharing.directory** - Specifies the directory to use for sharing storage. Defaults to `sharing`.

The `web.lofi.*` and `web.rest.*` configurations may be overriden in a
configuration profile.