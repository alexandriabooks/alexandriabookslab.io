---
title: Screenshots
subtitle:
comments: false
---

![LoFi Interface](../../screenshots/lofi-2020-02-11.jpg)

Above is the LoFi interface. It is completely static with simple HTML for use
on device browsers with minimal capabilities. The download sources with
asterisk mean the book with be automatically converted to that format from the
source format. You want to read a comic in CBZ, but your ebook reader only
supports EPUB? No problem!

![LoFi Icon Layout](../../screenshots/lofi_icon-2020-02-11.jpg)

For devices with a bit more power, an icon interface is available.

![OPDS on MoonReader+](../../screenshots/moonreader-2021-02-11.jpg)

Or checkout your libary via your favorite mobile app via the OPDS feed.

![Kobo eReader on LoFi](../../screenshots/kobo-2021-02-12.jpg)

The LoFi (low fidelity) interface is perfect for eInk devices with a built in
web browser, such as the Kobo. You can even configure the interface to
customize itself to the device accessing it. In the above screenshot, the
images are made greyscale and resized to fit the thumbnail size, so the 
eReader doesn't need to do much work. Additionally, only the formats the device
supports are shown as download links. The comic book (which was originally a
CBZ) will automatically be transcoded (converted) to an ePUB on download.
