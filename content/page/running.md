---
title: Running
subtitle: How to get started running Alexandria Books
comments: false
---

### Via Docker

    docker run -v /my/data/dir:/work -p 8080:8080 -d registry.gitlab.com/alexandriabooks/docker:latest

Command line options (such as logging) may be specified after the image name.
Use `--help` for more information.

Configuration may be specified as additional `-e` arguements, or via a
configuration file named `alexandria.yml` in the work directory. See
configuration section for more details.

See [Configuration](../config/) for more details on configuration.

Open a browser to [http://localhost:8080/](http://localhost:8080/)

**Note:** The docker image (by default), looks for a directory named `books`
under the mounted volume. If it finds it, it will index any book files it
finds and understands.

### Logging Options

Logging options are described in the `--help` text. Briefly, there are three
options for controlling logging:

* `-v [LEVEL]` changes the global logging level. (ie. `-v DEBUG`)
* `--logger-level [LOGGER_NAME] [LEVEL]` changes the level of a specific
logger. (ie. `--logger-level alexandriabooks_core.services.registry DEBUG`)
* `--log-format [FORMAT STRING]` sets the logging format. See
[python logging attributes](https://docs.python.org/3/library/logging.html#logrecord-attributes)
for more details.

Valid levels are: `DEBUG`, `INFO`, `WARNING`, `ERROR`, `FATAL` and `CRITICAL`

For example to set the global logging level to WARNING, but the registry
logging level to DEBUG and running the docker container, you would run:

    docker run -v /my/data/dir:/work -p 8080:8080 -d registry.gitlab.com/alexandriabooks/docker:latest -v WARNING --logger-level alexandriabooks_core.services.registry DEBUG run

**Note:** At the end of the command line is `run` which actually starts the server.

## Building the Docker image locally

Clone the [docker subproject](https://gitlab.com/alexandriabooks/docker), and
in the root of the project run:

    docker build --pull -t registry.gitlab.com/alexandriabooks/docker:latest .

## Endpoints

The server has different endpoints for serving your content in different ways.

* **/lofi** - This is a light weight interface for the lowest powered devices with a browser (ie. an e-ink eBook reader).
* **/hifi** - *(alpha)* This interface (enabled via the `web.hifi.path` setting, and enabled by default in the docker container) provides a much nicer interface, but requires more system resources.
* **/api/opds/root.xml** - This interface is for applications that support the Open Publication Distribution System (OPDS) specification.

### Developer Endpoints

There are also several endpoints interesting to developers building their own HiFi interface:

* **/api/graphql** - *(beta)* Provides a GraphQL API for interface developers. You can explore it with the intergrated GraphQL browser at **/api/graphql**.
* **/api/book/{book_id}/cover** - Downloads the cover for a book.
* **/api/source/{source_key}/cover** - Downloads the cover for a book's source.
* **/api/source/{source_key}/download** - Downloads a version of the book.
* **/api/source/{source_key}/transcode/{dest_type}** - Downloads a transcoded version of the book.
