---
title: Features
subtitle: What is Alexandia Books capable of?
comments: false
---

Alexandria Books is plugin based, which means most the functionality is
provided by plugins. This page will break down each plugin, and the
features it provides. The docker container built for this project
already contains every plugin from this project.

## Plugin Types

There are several types of plugins available:

- **Book Metadata Service** - Provides the eBook metadata storage.
- **Cache Service** - Provides a caching API for quick retrieval of common used
resources (such as resized cover images, or content fetched over HTTP).
- **Book Index Service** - Provides a searchable index of a book repository. This
may be a remote repository, or an index over the local Book Metadata Service.
- **Book Source Provider Service** - Handles the retrieval of eBooks from a source,
such as your local harddrive.
- **Image Transcoder Service** - Handles scaling and conversion of image files.
- **Audio Transcoder Service** - Handles the conversion of audio files.
- **Book Transcoder Service** - Handles the conversion of eBooks. A book transcoder
may make use of the image and audio transcoders, depending on the use case.
- **Book Parser Service** - Provides metadata about a provided book. This is where
new book types and formats can be supported.
- **Background Service** - Simple API to control background, long running
processes.
- **Cli Command** - Adds new commands to the Alexandria Books command line
interface.

## Supported eBook Types (via plugins)

### Traditional eBooks

eBook Type | Plugin             | Supported Conversions
---------- | ------------------ | ---------------------
ePUB       | LibArchive         | N/A

### Digital Comic Books

eBook Type | Plugin             | Supported Conversions
---------- | ------------------ | ---------------------
CBZ        | LibArchive         | ePUB, CBZ, CBT, CB7
CBT        | LibArchive         | ePUB, CBZ, CBT, CB7
CB7        | LibArchive         | ePUB, CBZ, CBT, CB7
CBA        | LibArchive/Acefile | ePUB, CBZ, CBT, CB7
CBR        | LibArchive         | ePUB, CBZ, CBT, CB7
PDF        | Pdf2Image          | CBZ, CBT, CB7

Via an image transcoder plugin (such as the one included `Pillow` project),
part of the conversion may include resizing all the images to fit within a
given width/height or even converting the images to greyscale.

### Audio Books

**Note:** *Audio Book format is relatively new, YMMV*.

eBook Type | Plugin             | Supported Conversions
---------- | ------------------ | -----------------------------
FLAC       | TagLib             | FLAC, M4A, MP3, OGG, WAV, WMA
M4A        | TagLib             | FLAC, M4A, MP3, OGG, WAV, WMA
MP3        | TagLib             | FLAC, M4A, MP3, OGG, WAV, WMA
OGG        | TagLib             | FLAC, M4A, MP3, OGG, WAV, WMA
WAV        | TagLib             | FLAC, M4A, MP3, OGG, WAV, WMA
WMA        | TagLib             | FLAC, M4A, MP3, OGG, WAV, WMA

Via the `LibArchive` plugin, the above formats are also supported when zipped in
an archive.

## Core Project

The core project provides the data model and API for interacting with
Alexandria Books. It also provide the minimum in-memory implementations of
the Metadata, Indexer and Cache services. The in-memory Metadata service,
if configured, can also snapshot to disk and read those snapshots on start up.

## Formats Project

The format project provides book parsers for basic eBook metadata. It can parse
metadata from EPUB files, as well as comic metadata including Advanced Comic
Book Format (ACBF), Droid Comic metadata, Comic Rack metadata, and Comic Book
Lover metadata.

It also provides a CBZ to ePUB book transcoder.

## API Server Project

The API Server project provides a low fidelity (and optionally hosts an
external high fidelity) web interface for a user to browse, search and download
their books.

It has REST APIs for the retrieval of book covers and books, optionally running
through a converted before being served to the user.

Additionally it provides a [OPDS](https://opds.io/) feed to expose the book
library to compatible applications.

Finally in order to support modern high fidelity interfaces, it provides a
complete GraphQL interface for searching and browsing all the eBook metadata.

## Ace File Project

It wraps the `acefile` library provides support for reading and parsing CBA
files.

## FFmpeg Project

The FFmpeg project uses a locally installed copy of FFmpeg to provide a audio
transcoder plugin. This allows indexed audio books that are FLAC (or another
supported audio) to be converted to MP3 or WAV on the fly to ensure
compatibility with difference devices and only requiring a single copy of the
files to be stored locally.

## Libarchive Project

This project wraps `libarchive` and provides parses for book formats based on
archive formats supported by libarchive. This includes: EPUB, CBZ, CBT, CBR,
CB7, CBA and audio books that are zipped archives of audio files.

It can also transcode any supported archive format to CBZ, CBT or CB7 formats.

## Local Project

Provides a source provider plugin finds eBooks on your local harddrive.

It also provides a cache service that writes the cached resources to disk. It
is the default implementation in the Docker image for caching.

## Pdf2Image Project

This project wraps the `Pdf2Image` project to provide a transcoder that can
convert PDF files into CBZ files.

It also provides a book parser for PDF files, providing minimal metadata
and cover images.

## Pillow Project

This project wraps the `pillow` library to provide an image transcoder service.
This means that cover images can be provided over the web interface in exactly
the right size and format for the device using the interface. Additionally for
devices with smaller resolutions then images in a comic book file, it can
resize the pages to save space and CPU time on the device. Why store more
pixels then you can actually render?

## Taglib Project

This project wraps the `taglib` library to provide metadata about audio book
files.

## Docker Project

This project takes the builds from all the above projects and packages them up
in a single, simple Docker image. Currently the Docker image is built for the
following CPU types:
- x86_64
- armv7l (ie. 32bit Raspberry Pi and similar boards)
- arm64 (ie. 64bit Raspberry Pi, Jetson Nano and similar boards)
